package com.alberto.practica1.Base;

import java.time.LocalDate;

public abstract class Semilla {
    private int id;
    private String nombre;
    private String color;
    private LocalDate fechaPlantacion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Constructor que inicializara los campos de semilla
     * @param nombre nombre de la semilla
     * @param color color de la semilla
     * @param fechaPlantacion fecha de plantacion de la semilla
     * @param id id de la semilla
     */
    public Semilla(String nombre, String color, LocalDate fechaPlantacion, int id) {
        this.nombre = nombre;
        this.color = color;
        this.fechaPlantacion = fechaPlantacion;
        this.id=id;
    }
    public Semilla(){

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public LocalDate getFechaPlantacion() {
        return fechaPlantacion;
    }

    public void setFechaPlantacion(LocalDate fechaPlantacion) {
        this.fechaPlantacion = fechaPlantacion;
    }
}
