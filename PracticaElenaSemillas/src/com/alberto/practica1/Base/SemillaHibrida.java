package com.alberto.practica1.Base;

import com.alberto.practica1.Base.Semilla;

import java.time.LocalDate;

public class SemillaHibrida extends Semilla {
    private String mezcla;

    /**
     * Metodo que inicializara los campos de semilla Hibrida
     * @param nombre nombre de la semilla
     * @param color color de la semilla
     * @param fechaPlantacion fecha de plantacion de la semilla
     * @param id id de la semilla
     * @param mezcla mezcla de la semilla
     */
    public SemillaHibrida(String nombre, String color, LocalDate fechaPlantacion,int id,String mezcla) {
        super(nombre, color, fechaPlantacion,id);
        this.mezcla = mezcla;
    }

    public SemillaHibrida() {
        super();
    }

    public String getMezcla() {
        return mezcla;
    }

    public void setMezcla(String mezcla) {
        this.mezcla = mezcla;
    }

    @Override
    public String toString() {
        return "SemillaHibrida{" +
                "nombre=" + getNombre() +
                "fechaPlantacion=" + getFechaPlantacion() +
                "color=" + getColor() +
                "mezcla" + getMezcla() +
                '}';
    }
}
