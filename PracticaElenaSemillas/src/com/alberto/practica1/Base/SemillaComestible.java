package com.alberto.practica1.Base;

import com.alberto.practica1.Base.Semilla;

import java.time.LocalDate;

public class SemillaComestible extends Semilla {
    private double calorias;

    /**
     * Constructor que inicializa los atributos
     * @param nombre El nombre de la semilla
     * @param color Color de la semilla
     * @param fechaPlantacion fecha de plantacion de la semilla
     * @param id id de la semilla
     * @param calorias calorias de la semilla
     */
    public SemillaComestible(String nombre, String color, LocalDate fechaPlantacion,int id ,double calorias) {
        super(nombre, color, fechaPlantacion,id);
        this.calorias = calorias;
    }

    public SemillaComestible() {
        super();
    }

    public double getCalorias() {
        return calorias;
    }

    public void setCalorias(double calorias) {
        this.calorias = calorias;
    }

    @Override
    public String toString() {
        return "SemillaComestible{" +
                "nombre=" + getNombre() +
                "fechaPlantacion=" + getFechaPlantacion() +
                "color=" + getColor() +
                "aporte" + getCalorias() +
                '}';
    }
}
