package com.alberto.practica1.Gui;

import com.alberto.practica1.Base.Semilla;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

public class Ventana {
    private JPanel panel;
    public JRadioButton comestibleRbt;
    public JRadioButton hibridaRdt;
    public JTextField nombre;
    public JTextField mezclaAporteNutri;
    public JTextField color;
    public JButton nuevaButton;
    public JButton importarButton;
    public JButton exportarButton;
    public DatePicker fechaPlantacionDpicker;
    public JList list1;
    public JLabel mezclaAporteLabel;
    public JTextField idSemilla;
    public JButton cerrarButton;
    public JFrame frame;
    public DefaultListModel<Semilla> dlmSemilla;

    /**
     * Constructor que inicializa la Ventana
     */
    public Ventana() {
        frame = new JFrame("SemillasMVC");
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        iniciarComponentes();
    }

    /**
     * Metodo que inicializara el defaultModelList y lo asigna a list1
     */
    private void iniciarComponentes(){
        dlmSemilla = new DefaultListModel<Semilla>();
        list1.setModel(dlmSemilla);


    }
}
