package com.alberto.practica1.Gui;

import com.alberto.practica1.Base.Semilla;
import com.alberto.practica1.Base.SemillaComestible;
import com.alberto.practica1.Base.SemillaHibrida;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class SemillaModelo {
    private ArrayList<Semilla> listaSemilla;

    public SemillaModelo() {
        listaSemilla = new ArrayList<Semilla>();
    }

    public ArrayList<Semilla> obtenerSemilla() {
        return listaSemilla;
    }

    /**
     * Metodo que da de alta una semilla hibrida
     * @param nombre  nombre de la semilla
     * @param color color de la semilla
     * @param fechaPlantacion fecha de plantacion de la semilla
     * @param id id de la semilla
     * @param mezcla mezcla de la semilla
     */
    public void altaSemillaHibrida(String nombre, String color, LocalDate fechaPlantacion, int id, String mezcla) {
        SemillaHibrida hibrida = new SemillaHibrida(nombre, color, fechaPlantacion, id, mezcla);
        listaSemilla.add(hibrida);
    }

    /**
     * Metodo que da de alta una semilla Comestible
     * @param nombre  nombre de la semilla
     * @param color color de la semilla
     * @param fechaPlantacion fecha de plantacion de la semilla
     * @param id id de la semilla
     * @param calorias calorias de la semilla
     */
    public void altaSemillaComestible(String nombre, String color, LocalDate fechaPlantacion, int id, int calorias) {
        SemillaComestible comestible = new SemillaComestible(nombre, color, fechaPlantacion, id, calorias);
        listaSemilla.add(comestible);

    }

    /**
     * Metodo que comprueba si existe el Id de una semilla
     * @param id id de la semilla
     * @return retornara true si existe o false si no existe
     */
    public boolean existeId(int id) {
        for (Semilla unaSemilla : listaSemilla) {
            if (unaSemilla.getId() == id) {
                return true;
            }
        }
        return false;
    }

    /**
     * Metodo que generara el XML de las semillas
     * @param fichero fichero que recibe
     *  Excepciones
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);

        Element raiz = documento.createElement("Semillas");
        documento.getDocumentElement().appendChild(raiz);
        Element nodoSemilla = null;
        Element nodoDato = null;
        Text texto = null;
        for (Semilla unaSemilla : listaSemilla) {
            if (unaSemilla instanceof SemillaComestible) {
                nodoSemilla = documento.createElement("SemillaComestible");
            } else {
                nodoSemilla = documento.createElement("SemillaHibrida");
            }
            raiz.appendChild(nodoSemilla);
            /*--------------------------------------------------------------------*/
            //Campos de las Semillas
            /*Nombre*/
            nodoDato = documento.createElement("nombre");
            nodoSemilla.appendChild(nodoDato);
            texto = documento.createTextNode(unaSemilla.getNombre());
            nodoDato.appendChild(texto);
            /*Color*/
            nodoDato = documento.createElement("color");
            nodoSemilla.appendChild(nodoDato);
            texto = documento.createTextNode(unaSemilla.getColor());
            nodoDato.appendChild(texto);
            /*Fecha Plantacion*/
            nodoDato = documento.createElement("fechaPlantacion");
            nodoSemilla.appendChild(nodoDato);
            texto = documento.createTextNode(unaSemilla.getFechaPlantacion().toString());
            nodoDato.appendChild(texto);

            if (unaSemilla instanceof SemillaComestible) {
                nodoDato = documento.createElement("calorias");
                nodoSemilla.appendChild(nodoDato);
                texto = documento.createTextNode(String.valueOf(((SemillaComestible) unaSemilla).getCalorias()));
                nodoDato.appendChild(texto);
            } else {
                nodoDato = documento.createElement("mezcla");
                nodoSemilla.appendChild(nodoDato);
                texto = documento.createTextNode(((SemillaHibrida) unaSemilla).getMezcla());
                nodoDato.appendChild(texto);
            }
            Source source = new DOMSource(documento);
            Result resultado = new StreamResult(fichero);
            Transformer transforme = TransformerFactory.newInstance().newTransformer();
            transforme.transform(source, resultado);

        }
    }

    /**
     * Metodo que mostrara el archivo XML que hayas seleccionado
     * @param fichero fichero que recibe
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException {
        listaSemilla = new ArrayList<Semilla>();
        SemillaHibrida nuevaHibrida=null;
        SemillaComestible nuevaComestible=null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        NodeList listaElementos = documento.getElementsByTagName("*");

        for (int i = 0; i < listaElementos.getLength(); i++) {
            Element nodoSemilla = (Element) listaElementos.item(i);
            if (nodoSemilla.getTagName().equals("SemillaHibrida")) {
                nuevaHibrida = new SemillaHibrida();
                nuevaHibrida.setNombre(nodoSemilla.getChildNodes().item(0).getTextContent());
                nuevaHibrida.setColor(nodoSemilla.getChildNodes().item(1).getTextContent());
                nuevaHibrida.setFechaPlantacion(LocalDate.parse(nodoSemilla.getChildNodes().item(2).getTextContent()));
                nuevaHibrida.setMezcla(nodoSemilla.getChildNodes().item(3).getTextContent());
                listaSemilla.add(nuevaHibrida);
            } else {
                if (nodoSemilla.getTagName().equals("SemillaComestible")) {
                    nuevaComestible = new SemillaComestible();
                    nuevaComestible.setNombre(nodoSemilla.getChildNodes().item(0).getTextContent());
                    nuevaComestible.setColor(nodoSemilla.getChildNodes().item(1).getTextContent());
                    nuevaComestible.setFechaPlantacion(LocalDate.parse(nodoSemilla.getChildNodes().item(2).getTextContent()));
                    nuevaComestible.setCalorias(Double.parseDouble(nodoSemilla.getChildNodes().item(3).getTextContent()));
                    listaSemilla.add(nuevaComestible);
                }
            }


        }


    }

}
