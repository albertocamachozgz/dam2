package com.alberto.practica1.Gui;

import com.alberto.practica1.Base.Semilla;
import com.alberto.practica1.Base.SemillaComestible;
import com.alberto.practica1.Base.SemillaHibrida;
import com.alberto.practica1.Util.Util;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.util.Properties;

public class SemillaControlador implements ActionListener, ListSelectionListener, WindowListener {
    private Ventana vista;
    private SemillaModelo modelo;
    private File ultimaRutaExportada;

    /**
     * Constructor que inicializa los campos
     * @param vista Instancia de la clase Ventana
     * @param modelo Instancia de la clase SemillaModelo
     */
    public SemillaControlador(Ventana vista, SemillaModelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        try {
            cargarDatosConfiguracion();
        } catch (IOException e) {
            System.out.println("No existe el fichero de configuracion " + e.getMessage());
        }
        addActionListener(this);
        addListSelectionListener(this);
        addWindowsListener(this);

    }

    /**
     * Metodo que a partir de la ultima ruta exportada abrira la ubicacion
     * @throws IOException
     */
    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        /*Load utiliza la carpeta que he guardado en la ultima ruta exportada*/
        configuracion.load(new FileReader("semillas.conf"));
        ultimaRutaExportada = new File(configuracion.getProperty("ultimaRutaExportada"));
    }

    /**
     * Metodo que actualiza la ruta de exportacion
     * @param ultimaRutaExportada  ruta exportada
     */
    private void actualizarDatosConfiguracion(File ultimaRutaExportada) {
        this.ultimaRutaExportada = ultimaRutaExportada;
    }

    /**
     * Metodo que almacenara en el fichero la ruta exportacion
     * @throws IOException
     */
    private void guardarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.setProperty("ultimaRutaExportada", ultimaRutaExportada.getAbsolutePath());
        configuracion.store(new PrintWriter("semillas.conf"), "Datos configuracion semillas");
    }

    /**
     * Metodo que dara de alta una semilla,importara o exportara
     * @param e recibira la opcion elegida en la interfaz
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();
        switch (actionCommand) {
            case "Nueva judia":
                if (hayCamposVacios()) {
                    Util.mensajeError("Los siguientes campos no pueden estar vacios \n" +
                            "Matricula \n Marca \n Modelo \n Fecha matriculación\n");
                    //vista.mezclaAporteNutri.getText());
                    break;
                }
                if (modelo.existeId(Integer.parseInt(vista.idSemilla.getText()))) {
                    Util.mensajeError("Ya existe una semilla con este Id");
                    //vista.mezclaAporteNutri.getText());
                    break;
                }
                if (vista.comestibleRbt.isSelected()) {
                    modelo.altaSemillaComestible(vista.nombre.getText(), vista.color.getText(), vista.fechaPlantacionDpicker.getDate(),
                            Integer.parseInt(vista.idSemilla.getText()), Integer.parseInt(vista.mezclaAporteNutri.getText()));
                } else {
                    modelo.altaSemillaHibrida(vista.nombre.getText(), vista.color.getText(), vista.fechaPlantacionDpicker.getDate(),
                            Integer.parseInt(vista.idSemilla.getText()), vista.mezclaAporteNutri.getText());
                }
                limpiarCampos();
                refrescar();
                break;

            case "Importar":
                JFileChooser selectorFichero = Util.crearSelectorFichero(ultimaRutaExportada,"Archivos XML","xml");
                int opt = selectorFichero.showOpenDialog(null);
                if(opt==JFileChooser.APPROVE_OPTION){
                    try {
                        modelo.importarXML(selectorFichero.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (SAXException ex) {
                        ex.printStackTrace();
                    }
                    refrescar();
                }
                break;
            case "Exportar":
                JFileChooser selectorFichero2 = Util.crearSelectorFichero(ultimaRutaExportada,"Archivos XML","xml");
                int op2 = selectorFichero2.showSaveDialog(null);
                if(op2==JFileChooser.APPROVE_OPTION){
                    try {
                        modelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (TransformerException ex) {
                        ex.printStackTrace();
                    }
                }
                break;
            case "SemillaComestibleRadioBtn":
                vista.mezclaAporteLabel.setText("AporteN");
                break;
            case"Semilla híbrida":
                vista.mezclaAporteLabel.setText("Mezcla");
                break;
            case "Cerrar":
                int resp=Util.mensajeConfirmacion("¿Desea cerrar la ventana","Salir");
                if (resp==JOptionPane.OK_OPTION){
                    try {
                        guardarDatosConfiguracion();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    System.exit(0);
                }
                    break;
        }
    }

    /**
     * Metodo que limpiara los campos de la interfaz
     */
    private void limpiarCampos() {
        vista.idSemilla.setText(null);
        vista.nombre.setText(null);
        vista.fechaPlantacionDpicker.setText(null);
        vista.color.setText(null);
        vista.mezclaAporteNutri.setText(null);
        vista.idSemilla.requestFocus();
    }

    /**
     * Metodo que eliminara de defaultListModel el contenido y lo volvera a añadir para que no se repitan los datos
     */
    private void refrescar() {
        vista.dlmSemilla.clear();
        for (Semilla unaSemilla : modelo.obtenerSemilla()) {
            vista.dlmSemilla.addElement(unaSemilla);
        }
    }

    /**
     * Metodo que comprobara que no haya campos vacios
     * @return
     */
    private boolean hayCamposVacios() {
        if (vista.nombre.getText().isEmpty() || vista.color.getText().isEmpty() || vista.fechaPlantacionDpicker.getText().isEmpty()
                || vista.mezclaAporteNutri.getText().isEmpty() || vista.idSemilla.getText().isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * Metodo donde se realizaran las funciones de cada boton
     * @param listener
     */
    private void addActionListener(ActionListener listener){
        vista.comestibleRbt.addActionListener(listener);
        vista.hibridaRdt.addActionListener(listener);
        vista.nuevaButton.addActionListener(listener);
        vista.importarButton.addActionListener(listener);
        vista.exportarButton.addActionListener(listener);
        vista.cerrarButton.addActionListener(listener);
    }

    /**
     * Metodo que dara funcionalidad al frame
     * @param listener
     */
    private void addWindowsListener(WindowListener listener){
    vista.frame.addWindowListener(listener);
    }

    /**
     * Metodo que dara funcionalidad a la lista
     * @param listener
     */
    private void addListSelectionListener(ListSelectionListener listener) {
        vista.list1.addListSelectionListener(listener);
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    /**
     * Metodo que se encargara de cerrar la ventana
     * @param e
     */
    @Override
    public void windowClosing(WindowEvent e) {
        int resp=Util.mensajeConfirmacion("¿Desea cerrar la ventana","Salir");
        if (resp==JOptionPane.OK_OPTION){
            try {
                guardarDatosConfiguracion();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            System.exit(0);
        }

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    /**
     * Metodo que, al pinchar en un campo de la lista los mostrara en los cuadros de texto de la interfaz
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()){
        Semilla semillaSeleccionada = (Semilla) vista.list1.getSelectedValue();
        vista.nombre.setText(semillaSeleccionada.getNombre());
        vista.color.setText((semillaSeleccionada.getColor()));
        vista.fechaPlantacionDpicker.setDate(semillaSeleccionada.getFechaPlantacion());
        vista.idSemilla.setText("");
            if (semillaSeleccionada instanceof SemillaComestible){
                vista.comestibleRbt.doClick();
                vista.mezclaAporteNutri.setText(String.valueOf(((SemillaComestible) semillaSeleccionada).getCalorias()));
            }else{
                vista.hibridaRdt.doClick();
                vista.mezclaAporteNutri.setText(((SemillaHibrida) semillaSeleccionada).getMezcla());
            }
        }
    }
}
