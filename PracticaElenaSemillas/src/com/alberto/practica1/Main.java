package com.alberto.practica1;

import com.alberto.practica1.Gui.SemillaControlador;
import com.alberto.practica1.Gui.SemillaModelo;
import com.alberto.practica1.Gui.Ventana;

/**
 *Hecho por Alberto Camacho Perez
 * Dia 08/11/2021
 */
public class Main {

    public static void main(String[] args) {
        Ventana vista = new Ventana();
        SemillaModelo modelo = new SemillaModelo();
        SemillaControlador controlador = new SemillaControlador(vista,modelo);
    }
}
