package com.alberto.practica1.Util;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

public class Util {
    /**
     * Metodo que mostrara en caso de erro un mensaje
     * @param mensaje mensaje que recibe por parametro
     */
    public static void mensajeError(String mensaje){
        JOptionPane.showMessageDialog(null,mensaje,"Error",JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Metodo que en caso de confirmacion realizara una accion
     * @param mensaje
     * @param titulo
     * @return
     */
    public static int mensajeConfirmacion(String mensaje, String titulo) {
        return JOptionPane.showConfirmDialog(null,mensaje,
                titulo,JOptionPane.YES_NO_OPTION);
    }

    /**
     * Metodo que creara el selector de fichero, ventana que seleccionara los ficheros
     * @param rutaDefecto
     * @param tipoArchivo
     * @param extension
     * @return
     */
    public static JFileChooser crearSelectorFichero(File rutaDefecto,String tipoArchivo,String extension){

        JFileChooser selectorFichero = new JFileChooser();
        if (rutaDefecto!=null){
            selectorFichero.setCurrentDirectory(rutaDefecto);
        }
        if(extension!=null ){
            FileNameExtensionFilter filtro = new FileNameExtensionFilter(tipoArchivo,extension);
            selectorFichero.setFileFilter(filtro);
        }
        return selectorFichero;
    }
}
