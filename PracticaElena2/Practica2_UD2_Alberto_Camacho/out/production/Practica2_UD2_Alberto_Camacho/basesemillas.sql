CREATE DATABASE basesemillas;
USE basesemillas;

CREATE TABLE proveedores(
id_proveedor INT AUTO_INCREMENT PRIMARY KEY,
nombre VARCHAR (30) NOT NULL,
codigo_postal INT NOT NULL,
ciudad varchar (40) NOT NULL,
provincia varchar (40) NOT NULL,
movil INT,
telefono int NOT NULL
);
CREATE TABLE agricultor(
id_agricultor INT AUTO_INCREMENT PRIMARY KEY,
nombre VARCHAR (30) NOT NULL,
codigo_postal INT NOT NULL,
ciudad VARCHAR (40) NOT NULL,
dni VARCHAR(9) NOT NULL,
id_proveedor INT,
FOREIGN KEY (id_proveedor) REFERENCES proveedores(id_proveedor)
);
CREATE TABLE semillas(
id_semilla INT AUTO_INCREMENT PRIMARY KEY,
tipo_semilla VARCHAR (30) NOT NULL,
precio_kilo float NOT NULL,
ciudad_plantada VARCHAR (40) NOT NULL,
fecha_plantacion TIMESTAMP,
id_agricultor INT,
FOREIGN KEY (id_agricultor) REFERENCES agricultor(id_agricultor)
);