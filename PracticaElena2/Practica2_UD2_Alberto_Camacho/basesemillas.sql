CREATE DATABASE basesemillas;
USE basesemillas;

CREATE TABLE proveedores(
id_proveedor INT AUTO_INCREMENT PRIMARY KEY,
nombre VARCHAR (30) NOT NULL,
codigo_postal INT NOT NULL,
comunidad varchar (40) NOT NULL,
provincia varchar (40) NOT NULL,
movil INT,
telefono INT NOT NULL
);
CREATE TABLE agricultor(
id_agricultor INT AUTO_INCREMENT PRIMARY KEY,
nombre VARCHAR (30) NOT NULL,
codigo_postal INT NOT NULL,
ciudad VARCHAR (40) NOT NULL,
dni VARCHAR(9) NOT NULL,
id_proveedor INT,
FOREIGN KEY (id_proveedor) REFERENCES proveedores(id_proveedor)
);
CREATE TABLE semillas(
id_semilla INT AUTO_INCREMENT PRIMARY KEY,
tipo_semilla VARCHAR (30) NOT NULL,
numero_serie INT,
precio_kilo float NOT NULL,
ciudad_plantada VARCHAR (40) NOT NULL,
fecha_plantacion date,
id_agricultor INT,
FOREIGN KEY (id_agricultor) REFERENCES agricultor(id_agricultor)
);
delimiter
create function existeMovilProveedor(f_movilprov INT)
returns bit
begin
    declare i int;
    set i = 0;
    while ( i < (select max(id_proveedor) from proveedores)) do
    if  ((select movil from proveedores where id_proveedor = (i + 1)) like f_movilprov) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end;
delimiter ;

delimiter
create function existeAgricultor(f_dni varchar(9))
returns bit
begin
    declare i int;
    set i = 0;
    while ( i < (select max(id_agricultor) from agricultor)) do
    if  ((select dni from agricultor where id_agricultor = (i + 1)) like f_dni) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end;
delimiter ;

delimiter
create function existeSemilla(f_numeroS INT)
returns bit
begin
    declare i int;
    set i = 0;
    while ( i < (select max(id_semilla) from semillas)) do
    if  ((select numero_serie from semillas where id_semilla = (i + 1)) like f_numeroS) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end;
delimiter ;