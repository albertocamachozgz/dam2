package enums;

public enum TiposSemillas {
    SEMILLASMEJORADAS("Mejorada"),
    SEMILLASBABY("Baby"),
    SEMILLASHIBRIDAS("Hibrida"),
    SEMILLASHORTALIZAS("Hortaliza"),
    SEMILLAFRUTAS("Fruta");

     private String tipoSemilla;
    TiposSemillas(String tipoS) {
        this.tipoSemilla=tipoS;
    }

    public String getTipoSemilla() {
        return tipoSemilla;
    }
}
