package gui;

import com.github.lgooddatepicker.components.DatePicker;
import enums.Ciudades;
import enums.Comunidades;
import enums.Provincias;
import enums.TiposSemillas;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Vista extends JFrame{
    private JPanel panel1;
    private JTabbedPane tabbedPane1;
    private JPanel PanelProveedor;
    private JPanel PanelAgricultor;
    private JPanel PanelSemilla;

    //PROVEEDOR
    private JTextField proveedorNombretxt;
    private JTextField codigoPostalProveedortxt;
    private JComboBox comunidadProveedorComboBox;
    private JComboBox provinciaProveedorComboBox;
    private JTextField movilProveedortxt;
    private JTextField telefonoProveedortxt;
    private JButton añadirProveedor;
    private JButton modificarProveedor;
    private JButton eliminarProveedor;
    private JTable tablaProveedores;
    private JComboBox comboIdProveedor;
    private JTextField dniAgricultor;
    private JComboBox ciudadAgricultorComboBox;
    private JTextField nombreAgricultor;
    private JTextField codigoPostalAgricultor;
    private JButton añadirAgricultor;
    private JButton modificarAgricultor;
    private JButton eliminarAgricultor;
    private JTable tablaAgricultor;




    //SEMILLA
    private JTextField precioKilosSemillatxt;
    private JComboBox tipoSemillaComboBox;
    // private JTextField ciudadPlantadaSemillatxt;
    private JButton añadirSemilla;
    private JButton modificarSemilla;
    private JButton eliminarSemilla;
    private DatePicker fechaPlantacion;
    private JTable tablaSemillas;
    private JComboBox ciudadSemillaPlantadaComboBox;
    private JTextField textField1;
    private JComboBox comboIdAgricultor;

    //COMPONENTES MENU
    JMenuItem opciones;
    JMenuItem desconectar;
    JMenuItem salir;

    //COMPONENTES OPTIONDIALOG
    OptionDialog optionDialog;
    JDialog adminPasswordDialog;
    JButton botonValidar;
    JPasswordField adminPassword;

    //DEFAULT TABLE MODEL
    DefaultTableModel dtmSemillas;
    DefaultTableModel dtmAgricultor;
    DefaultTableModel dtmProveedor;

    public Vista() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(700,500);
        this.setVisible(true);
        componentes();

    }
    public void componentes(){
        rellenarComboBox();
        menu();
        setAdminDialog();
        setTableModels();
    }

    public void setAdminDialog(){

    }
    public void setTableModels(){
        dtmSemillas = new DefaultTableModel();
        tablaSemillas.setModel(dtmSemillas);
        dtmAgricultor = new DefaultTableModel();
        tablaAgricultor.setModel(dtmAgricultor);
        dtmProveedor = new DefaultTableModel();
        tablaProveedores.setModel(dtmProveedor);

    }

    public void menu(){
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        opciones = new JMenuItem("Opciones");
        opciones.setActionCommand("Opciones");
        desconectar = new JMenuItem("Desconectar");
        desconectar.setActionCommand("Desconectar");
        salir = new JMenuItem("Salir");
        salir.setActionCommand("Salir");
        menu.add(opciones);
        menu.add(desconectar);
        menu.add(salir);
        barra.add(menu);
        this.setJMenuBar(barra);
    }
    public void rellenarComboBox(){
        for (Comunidades comun : Comunidades.values()) {

            comunidadProveedorComboBox.addItem(comun.getComunidades());
        }
        comunidadProveedorComboBox.setSelectedIndex(-1);
        for (Ciudades ciudad : Ciudades.values()){
            ciudadAgricultorComboBox.addItem(ciudad.getCiudad());
            ciudadSemillaPlantadaComboBox.addItem(ciudad.getCiudad());

        }
        ciudadAgricultorComboBox.setSelectedIndex(-1);
        ciudadSemillaPlantadaComboBox.setSelectedIndex(-1);
        for (TiposSemillas tSemilla : TiposSemillas.values()){
            tipoSemillaComboBox.addItem(tSemilla.getTipoSemilla());
        }
        tipoSemillaComboBox.setSelectedIndex(-1);
        for (Provincias prov : Provincias.values()){
            provinciaProveedorComboBox.addItem(prov.getProvincia());
        }
        provinciaProveedorComboBox.setSelectedIndex(-1);
    }

}
