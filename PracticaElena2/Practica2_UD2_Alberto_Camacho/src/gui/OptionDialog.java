package gui;

import javax.swing.*;
import java.awt.*;

public class OptionDialog extends JDialog{
    private JPanel panel1;
    private JTextField textIp;
    private JTextField textUsuario;
    private JPasswordField contrasenna;
    private JPasswordField contrasennaadmin;
    private JButton btnGuardar;

    private Frame prop;
    public OptionDialog(Frame propietario){
     super(propietario,"Opciones",true);
     this.prop=propietario;
     iniciarDialog();
    }

    public void iniciarDialog() {
        this.setContentPane(panel1);
        this.panel1.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth()+200,this.getHeight()));
        this.setLocationRelativeTo(prop);
    }
}
