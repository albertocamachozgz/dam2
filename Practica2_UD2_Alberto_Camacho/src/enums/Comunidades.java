package enums;

public enum Comunidades {
    COMUNIDADARAGON("Aragon"),
    COMUNIDADMADRID("Madrid"),
    COMUNIDADCATALUNNA("Cataluña");
    private String comunidades;
    Comunidades(String comun){
        this.comunidades=comun;
    }

    public String getComunidades() {
        return comunidades;
    }
}
