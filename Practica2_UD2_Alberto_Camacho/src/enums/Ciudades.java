package enums;

public enum Ciudades {
    ZARAGOZA("Zaragoza"),
    HUESCA("Huesca"),
    TERUEL("Teruel"),
    MADRID("Madrid"),
    BARCELONA("Barcelona");

    private String ciudad;
    Ciudades(String ciud){
        this.ciudad=ciud;
    }

    public String getCiudad() {
        return ciudad;
    }
}
