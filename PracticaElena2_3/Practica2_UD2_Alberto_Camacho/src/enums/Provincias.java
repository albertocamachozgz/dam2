package enums;

public enum Provincias {
    PROVZARAGOZA("Zaragoza"),
    PROVHUESCA("Huesca"),
    PROVTERUEL("Teruel"),
    PROVMADRID("Madrid"),
    PROVBARCELONA("Barcelona");

    private String provincia;
    Provincias(String prov){
        this.provincia=prov;
    }

    public String getProvincia() {
        return provincia;
    }
}
