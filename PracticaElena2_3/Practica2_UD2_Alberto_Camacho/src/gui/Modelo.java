package gui;

import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;

public class Modelo {

    private String ip;
    private String user;
    private String contrasenna;
    private String contrasennaAdmin;
    private Connection conexion;

    public String getIp() {
        return ip;
    }

    public String getUser() {
        return user;
    }

    public String getContrasenna() {
        return contrasenna;
    }

    public String getContrasennaAdmin() {
        return contrasennaAdmin;
    }
    public Modelo(){
        getPropValues();
    }
    /*INSERTAR PROVEEDOR*/
    public void insertarProveedor(String nombre,int codigoPs,String comunidad,String provincia,int movil,int tlf){
        String sentenciaSql ="INSERT INTO proveedores (nombre,codigo_postal,comunidad,provincia,movil,telefono) VALUES (?,?,?,?,?,?)";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1,nombre);
            sentencia.setInt(2,codigoPs);
            sentencia.setString(3,comunidad);
            sentencia.setString(4,provincia);
            sentencia.setInt(5,movil);
            sentencia.setInt(6,tlf);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (sentencia!=null){
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }
    /*INSERTAR AGRICULTOR*/
    public void insertarAgricultor(String nombre,int codigoPs,String ciudad,String dni,String id_prov){
        String sentenciaSql ="INSERT INTO agricultor (nombre,codigo_postal,ciudad,dni,id_proveedor) VALUES (?,?,?,?,?)";
        PreparedStatement sentencia = null;

        int idProve = Integer.valueOf(id_prov.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1,nombre);
            sentencia.setInt(2,codigoPs);
            sentencia.setString(3,ciudad);
            sentencia.setString(4,dni);
            sentencia.setInt(5,idProve);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (sentencia!=null){
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }
    /*INSERTAR SEMILLA*/
    public void insertarSemilla(String tipoSem, int numeroSerie, float precioKilo, String ciudadPlantada, LocalDate fechaPlantacion,String idAgri){
        String sentenciaSql ="INSERT INTO semillas (tipo_semilla,numero_serie,precio_kilo,ciudad_plantada,fecha_plantacion,id_agricultor) VALUES (?,?,?,?,?,?)";
        PreparedStatement sentencia = null;

        int idAgricu = Integer.valueOf(idAgri.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1,tipoSem);
            sentencia.setInt(2,numeroSerie);
            sentencia.setFloat(3,precioKilo);
            sentencia.setString(4,ciudadPlantada);
            sentencia.setDate(5, Date.valueOf(fechaPlantacion));
            sentencia.setInt(6,idAgricu);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (sentencia!=null){
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }
    //MODIFICAR PROVEEDOR
    public void modificarProveedor(String nombre,int codigoPs,String comunidad,String provincia,int movil,int tlf,int idProv){
        String sentenciaSql ="UPDATE proveedores SET nombre=?,codigo_postal=?,comunidad=?,provincia=?,movil=?,telefono=?"+ "WHERE id_proveedor=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1,nombre);
            sentencia.setInt(2,codigoPs);
            sentencia.setString(3,comunidad);
            sentencia.setString(4,provincia);
            sentencia.setInt(5,movil);
            sentencia.setInt(6,tlf);
            sentencia.setInt(7,idProv);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (sentencia!=null){
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }


    //MODIFICAR AGRICULTOR
    public void modificarAgricultor(String nombre,int codigoPs,String ciudad,String dni,int id_prov,int idAgri){
        String sentenciaSql ="UPDATE agricultor SET nombre=?,codigo_postal=?,ciudad=?,dni=?,id_proveedor=?"+ "WHERE id_agricultor=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1,nombre);
            sentencia.setInt(2,codigoPs);
            sentencia.setString(3,ciudad);
            sentencia.setString(4,dni);
            sentencia.setInt(5,id_prov);
            sentencia.setInt(6,idAgri);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (sentencia!=null){
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }
    //MODIFICAR SEMILLA
    public void modificarSemilla(String tipoS,int numSerie,float precioK,String ciudadPlantada,Date fechaPlant,int idAgri,int idSem){
        String sentenciaSql ="UPDATE semillas SET tipo_semilla=?,numero_serie=?,precio_kilo=?,ciudad_plantada=?,fecha_plantacion=?,id_agricultor=?"+ "WHERE id_semilla=?";

        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1,tipoS);
            sentencia.setInt(2,numSerie);
            sentencia.setFloat(3,precioK);
            sentencia.setString(4,ciudadPlantada);
            sentencia.setDate(5,fechaPlant);
            sentencia.setInt(6,idAgri);
            sentencia.setInt(7,idSem);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (sentencia!=null){
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }
    //BORRAR PROVEEDOR
    public void borrarProveedor(int idProv){
        String sentenciaSql = "DELETE FROM proveedores WHERE id_proveedor=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1,idProv);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
        if(sentencia!=null){
            try {
                sentencia.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        }

    }

    //BORRAR AGRICULTOR
    public void borrarAgricultor(int idAgri){
        String sentenciaSql = "DELETE FROM agricultor WHERE id_agricultor=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1,idAgri);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if(sentencia!=null){
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    //BORRAR SEMILLA
    public void borrarSemilla(int idSem){
        String sentenciaSql = "DELETE FROM semillas WHERE id_semilla=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1,idSem);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if(sentencia!=null){
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }
    //CONSULTAR PROVEEDOR
    ResultSet consultarProveedor() throws SQLException {
        String sentenciaSql = "SELECT concat(id_proveedor) AS 'ID', concat(nombre) AS 'Nombre proveedor', " +
                "concat(codigo_postal) AS 'Codigo Postal', concat(comunidad) AS 'Comunidad',concat(provincia) AS 'Provincia'," +
                "concat(movil) AS 'Movil', concat(telefono) AS 'Telefono' FROM proveedores";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }
    //CONSULTAR AGRICULTOR


    //CONSULTAR SEMILLA


    //EXISTE PROVEEDOR
    public boolean existeProveedor(int movil) {
        String consulta="SELECT existeMovilProveedor(?)";
        PreparedStatement function;
        boolean movilExists=false;
        try {
            function=conexion.prepareStatement(consulta);
            function.setInt(1,movil);
            ResultSet rs =function.executeQuery();
            rs.next();
            movilExists=rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return movilExists;
    }

    //EXISTE AGRICULTOR
    public boolean existeAgricultor(int dni) {
        String consulta="SELECT existeAgricultor(?)";
        PreparedStatement function;
        boolean dniExists=false;
        try {
            function=conexion.prepareStatement(consulta);
            function.setInt(1,dni);
            ResultSet rs =function.executeQuery();
            rs.next();
            dniExists=rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dniExists;
    }
//existeSemilla
    //EXISTE SEMILLA
    public boolean existeSemilla(int numSerie) {
    String consulta="SELECT existeSemilla(?)";
    PreparedStatement function;
    boolean numeroSerieExists=false;
    try {
        function=conexion.prepareStatement(consulta);
        function.setInt(1,numSerie);
        ResultSet rs =function.executeQuery();
        rs.next();
        numeroSerieExists=rs.getBoolean(1);
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return numeroSerieExists;
}


    public void conectar(){
        try{
            conexion = DriverManager.getConnection("jdbc:mysql://"+ip+":3306/basesemillas",user,contrasenna);
        }catch(SQLException e){
            try {
                conexion = DriverManager.getConnection("jdbc:mysql://"+ip+":3306/basesemillas",user,contrasenna);
                PreparedStatement statement = null;
                String code= leerFichero();
                String[] query = code.split("--");
                for(String aQuery : query){
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement!=null;
                statement.close();
            } catch (SQLException | IOException ex) {
                ex.printStackTrace();
            }

        }
    }
    public String leerFichero() throws IOException {
        try(BufferedReader reader = new BufferedReader(new FileReader("basesemillas_java.sql"))){
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while((linea = reader.readLine())!=null){
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }
            return stringBuilder.toString();
        }
    }
    public void desconectar(){
        try {
            conexion.close();
            conexion=null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void getPropValues(){
        InputStream inputStream = null;
        try{
        Properties prop= new Properties();
        String fileName = "config.properties";
        inputStream = new FileInputStream(fileName);
        prop.load(inputStream);
        ip = prop.getProperty("ip");
        user = prop.getProperty("user");
        contrasenna = prop.getProperty("pass");
        contrasennaAdmin = prop.getProperty("admin");

        }catch(IOException e){
            e.printStackTrace();
        }finally {
            if (inputStream!=null){
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void setPropValues(String ip,String user,String pass,String adminPass){
        try {
        Properties prop = new Properties();
        prop.setProperty("ip",ip);
        prop.setProperty("user",user);
        prop.setProperty("pass",pass);
        prop.setProperty("admin",adminPass);

            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out,null);

        } catch (IOException e) {
            e.printStackTrace();
        }
        this.ip=ip;
        this.user=user;
        this.contrasenna=pass;
        this.contrasennaAdmin=adminPass;
    }

}
