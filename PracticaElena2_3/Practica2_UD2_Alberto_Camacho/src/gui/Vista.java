package gui;

import com.github.lgooddatepicker.components.DatePicker;
import enums.Ciudades;
import enums.Comunidades;
import enums.Provincias;
import enums.TiposSemillas;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Vista extends JFrame{
    private JPanel panel1;
    private JTabbedPane tabbedPane1;
    private JPanel PanelProveedor;
    private JPanel PanelAgricultor;
    private JPanel PanelSemilla;

    //PROVEEDOR
    public JTextField proveedorNombretxt;
    public JTextField codigoPostalProveedortxt;
    public JComboBox comunidadProveedorComboBox;
    public JComboBox provinciaProveedorComboBox;
    public JTextField movilProveedortxt;
    public JTextField telefonoProveedortxt;
    public JButton añadirProveedor;
    public JButton modificarProveedor;
    public JButton eliminarProveedor;
    public JTable tablaProveedores;
    //AGRICULTOR
    public JComboBox comboIdProveedor;
    public JTextField dniAgricultor;
    public JComboBox ciudadAgricultorComboBox;
    public JTextField nombreAgricultor;
    public JTextField codigoPostalAgricultor;
    public JButton añadirAgricultor;
    public JButton modificarAgricultor;
    public JButton eliminarAgricultor;
    public JTable tablaAgricultor;




    //SEMILLA
    public JTextField precioKilosSemillatxt;
    public JComboBox tipoSemillaComboBox;
    public JButton añadirSemilla;
    public JButton modificarSemilla;
    public JButton eliminarSemilla;
    public DatePicker fechaPlantacion;
    public JTable tablaSemillas;
    public JComboBox ciudadSemillaPlantadaComboBox;
    public JTextField textField1;
    public JComboBox comboIdAgricultor;

    //COMPONENTES MENU
    JMenuItem opciones;
    JMenuItem desconectar;
    JMenuItem salir;

    //COMPONENTES OPTIONDIALOG
    OptionDialog optionDialog;
    JDialog adminPasswordDialog;
    JButton botonValidar;
    JPasswordField adminPassword;

    //DEFAULT TABLE MODEL
    DefaultTableModel dtmSemillas;
    DefaultTableModel dtmAgricultor;
    DefaultTableModel dtmProveedor;

    public Vista() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(700,500);
        this.setVisible(true);
        this.setLocationRelativeTo(this);

        componentes();

    }
    public void componentes(){
        optionDialog = new OptionDialog(this);
        rellenarComboBox();
        menu();
        setAdminDialog();
        setTableModels();
    }

    public void setAdminDialog(){
        botonValidar= new JButton("Validar");
        botonValidar.setActionCommand("abrirOpciones");
        adminPassword= new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100,26));
        Object[] options = new Object[] {adminPassword,botonValidar};
        JOptionPane jop =new JOptionPane("Introduce la contraseña",
                JOptionPane.WARNING_MESSAGE,JOptionPane.YES_NO_OPTION,null,options);
        adminPasswordDialog=new JDialog(this,"Opciones",true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);

    }
    public void setTableModels(){
        dtmSemillas = new DefaultTableModel();
        tablaSemillas.setModel(dtmSemillas);
        dtmAgricultor = new DefaultTableModel();
        tablaAgricultor.setModel(dtmAgricultor);
        dtmProveedor = new DefaultTableModel();
        tablaProveedores.setModel(dtmProveedor);

    }

    public void menu(){
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        opciones = new JMenuItem("Opciones");
        opciones.setActionCommand("Opciones");
        desconectar = new JMenuItem("Desconectar");
        desconectar.setActionCommand("Desconectar");
        salir = new JMenuItem("Salir");
        salir.setActionCommand("Salir");
        menu.add(opciones);
        menu.add(desconectar);
        menu.add(salir);
        barra.add(menu);
        this.setJMenuBar(barra);
    }
    public void rellenarComboBox(){
        for (Comunidades comun : Comunidades.values()) {

            comunidadProveedorComboBox.addItem(comun.getComunidades());
        }
        comunidadProveedorComboBox.setSelectedIndex(-1);
        for (Ciudades ciudad : Ciudades.values()){
            ciudadAgricultorComboBox.addItem(ciudad.getCiudad());
            ciudadSemillaPlantadaComboBox.addItem(ciudad.getCiudad());

        }
        ciudadAgricultorComboBox.setSelectedIndex(-1);
        ciudadSemillaPlantadaComboBox.setSelectedIndex(-1);
        for (TiposSemillas tSemilla : TiposSemillas.values()){
            tipoSemillaComboBox.addItem(tSemilla.getTipoSemilla());
        }
        tipoSemillaComboBox.setSelectedIndex(-1);
        for (Provincias prov : Provincias.values()){
            provinciaProveedorComboBox.addItem(prov.getProvincia());
        }
        provinciaProveedorComboBox.setSelectedIndex(-1);
    }

}
