package gui;

import Util.Util;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {
   private Modelo modelo;
   private Vista vista;
   boolean refrescar;
   public Controlador(Vista vista, Modelo modelo){
       this.modelo=modelo;
       this.vista=vista;
       modelo.conectar();
       addActionListeners(this);
       refrescarTodo();

   }
   public void refrescarTodo(){
       refrescarProveedor();
       refrescarAgricultor();
       refrescarSemilla();
       refrescar=false;
   }
   private void addActionListeners(ActionListener listener){
       //AÑADIR
       vista.añadirProveedor.addActionListener(listener);
       vista.añadirProveedor.setActionCommand("anadirProveedor");
       vista.añadirAgricultor.addActionListener(listener);
       vista.añadirAgricultor.setActionCommand("anadirAgricultor");
       vista.añadirSemilla.addActionListener(listener);
       vista.añadirSemilla.setActionCommand("anadirSemilla");
       //ELIMINAR
       vista.eliminarProveedor.addActionListener(listener);
       vista.eliminarProveedor.setActionCommand("eliminarProveedor");
       vista.eliminarAgricultor.addActionListener(listener);
       vista.eliminarAgricultor.setActionCommand("eliminarAgricultor");
       vista.eliminarSemilla.addActionListener(listener);
       vista.eliminarSemilla.setActionCommand("eliminarSemilla");
       //MODIFICAR
       vista.modificarProveedor.addActionListener(listener);
       vista.modificarProveedor.setActionCommand("modificarProveedor");
       vista.modificarAgricultor.addActionListener(listener);
       vista.modificarAgricultor.setActionCommand("modificarAgricultor");
       vista.modificarSemilla.addActionListener(listener);
       vista.modificarSemilla.setActionCommand("modificarSemilla");
       //OPTION DIALOG
       vista.optionDialog.btnGuardar.addActionListener(listener);
       vista.opciones.addActionListener(listener);
       vista.salir.addActionListener(listener);
       vista.botonValidar.addActionListener(listener);
   }
   private void addWindowsListeners(WindowListener listener){
       vista.addWindowListener(listener);
   }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch(command){
            case "Opciones":
                vista.adminPasswordDialog.setVisible(true);
                break;
            case "Desconectar":
                modelo.desconectar();
                break;
            case "Salir":
                System.exit(0);
                break;
            case "abrirOpciones":
                if (String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getContrasennaAdmin())) {
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);
                } else {
                    Util.showErrorAlert("La contraseña introducida no es correcta");
                }
                break;
            case "guardarOpciones":
                modelo.setPropValues(vista.optionDialog.textIp.getText(),
                        vista.optionDialog.textUsuario.getText(),
                        String.valueOf(vista.optionDialog.contrasenna.getPassword()),
                        String.valueOf(vista.optionDialog.contrasennaadmin.getPassword()));
                vista.optionDialog.dispose();
                vista.dispose();
                new Controlador(new Vista(), new Modelo());
                    break;
            case"anadirProveedor":
                break;
        }

    }
    //TABLE MODEL PROVEEDOR
    private DefaultTableModel construirTableModelProveedor(ResultSet rs) throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmProveedor.setDataVector(data, columnNames);
        return vista.dtmProveedor;
    }
    //TABLE MODEL AGRICULTOR
    private DefaultTableModel construirTableModelaGRICULTOR(ResultSet rs) throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmAgricultor.setDataVector(data, columnNames);
        return vista.dtmAgricultor;
    }
    //TABLE MODEL SEMILLA
    private DefaultTableModel construirTableModelSemilla(ResultSet rs) throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmSemillas.setDataVector(data, columnNames);
        return vista.dtmSemillas;
    }
    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }
    public void refrescarProveedor(){

    }
    public void refrescarAgricultor(){

    }
    public void refrescarSemilla(){

    }
    private void borrarCamposProveedor(){
       vista.proveedorNombretxt.setText("");
       vista.codigoPostalProveedortxt.setText("");
       vista.comunidadProveedorComboBox.setSelectedIndex(-1);
       vista.provinciaProveedorComboBox.setSelectedIndex(-1);
       vista.movilProveedortxt.setText("");
       vista.telefonoProveedortxt.setText("");
    }
    private void borrarCamposAgricultor(){

    }
    private void borrarCamposAutor(){

    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

    }
}
